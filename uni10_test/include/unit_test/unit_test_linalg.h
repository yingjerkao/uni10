#ifndef __UNIT_TEST_LINALG_H__
#define __UNIT_TEST_LINALG_H__

#include "uni10.hpp"
#include "test_tool.h"

template<typename uni10_type>
  void unitTestSVD(uni10::Matrix<uni10_type> &M, decltype(qr(M)) &result, Perf &performance)
  {
    performance.start();

    result = svd(M);

    performance.end();
  }

template<typename uni10_type>
  void unitTestSVDinplace(uni10::Matrix<uni10_type> &M, decltype(qr(M)) &result, Perf &performance)
  {
    performance.start();

    svd(M, result.at(0), result.at(1), result.at(2), uni10::INPLACE);

    performance.end();
  }

template<typename uni10_type>
  void unitTestSDD(uni10::Matrix<uni10_type> &M, decltype(qr(M)) &result, Perf &performance)
  {
    performance.start();

    result = sdd(M);

    performance.end();
  }

template<typename uni10_type>
  void unitTestSDDinplace(uni10::Matrix<uni10_type> &M, decltype(qr(M)) &result, Perf &performance)
  {
    performance.start();

    sdd(M, result.at(0), result.at(1), result.at(2), uni10::INPLACE);

    performance.end();
  }

template<typename uni10_type>
  void unitTestQR(uni10::Matrix<uni10_type> &M, decltype(qr(M)) &result, Perf &performance)
  {
    performance.start();

    result = qr(M);

    performance.end();
  }

template<typename uni10_type>
  void unitTestQRinplace(uni10::Matrix<uni10_type> &M, decltype(qr(M)) &result, Perf &performance)
  {
    performance.start();

    qr(M, result.at(0), result.at(1), uni10::INPLACE);

    performance.end();
  }

template<typename uni10_type>
  void unitTestQL(uni10::Matrix<uni10_type> &M, decltype(ql(M)) &result, Perf &performance)
  {
    performance.start();

    result = ql(M);

    performance.end();
  }

template<typename uni10_type>
  void unitTestQLinplace(uni10::Matrix<uni10_type> &M, decltype(ql(M)) &result, Perf &performance)
  {
    performance.start();

    ql(M, result.at(0), result.at(1), uni10::INPLACE);

    performance.end();
  }

template<typename uni10_type>
  void unitTestRQ(uni10::Matrix<uni10_type> &M, decltype(rq(M)) &result, Perf &performance)
  {
    performance.start();

    result = rq(M);

    performance.end();
  }

template<typename uni10_type>
  void unitTestRQinplace(uni10::Matrix<uni10_type> &M, decltype(rq(M)) &result, Perf &performance)
  {
    performance.start();

    rq(M, result.at(0), result.at(1), uni10::INPLACE);

    performance.end();
  }

template<typename uni10_type>
  void unitTestLQ(uni10::Matrix<uni10_type> &M, decltype(lq(M)) &result, Perf &performance)
  {
    performance.start();

    result = lq(M);

    performance.end();
  }

template<typename uni10_type>
  void unitTestLQinplace(uni10::Matrix<uni10_type> &M, decltype(lq(M)) &result, Perf &performance)
  {
    performance.start();

    lq(M, result.at(0), result.at(1), uni10::INPLACE);

    performance.end();
  }

template<typename uni10_type>
  void unitTestQDR(uni10::Matrix<uni10_type> &M, decltype(qdr(M)) &result, Perf &performance)
  {
    performance.start();

    result = qdr(M);

    performance.end();
  }

template<typename uni10_type>
  void unitTestQDRinplace(uni10::Matrix<uni10_type> &M, decltype(qdr(M)) &result, Perf &performance)
  {
    performance.start();

    qdr(M, result.at(0), result.at(1), result.at(2), uni10::INPLACE);

    performance.end();
  }

template<typename uni10_type>
  void unitTestLDQ(uni10::Matrix<uni10_type> &M, decltype(ldq(M)) &result, Perf &performance)
  {
    performance.start();

    result = ldq(M);

    performance.end();
  }

template<typename uni10_type>
  void unitTestLDQinplace(uni10::Matrix<uni10_type> &M, decltype(ldq(M)) &result, Perf &performance)
  {
    performance.start();

    ldq(M, result.at(0), result.at(1), result.at(2), uni10::INPLACE);

    performance.end();
  }

template<typename uni10_type>
  void unitTestQDRCPIVOT(uni10::Matrix<uni10_type> &M, decltype(qdr_cpivot(M)) &result, Perf &performance)
  {
    performance.start();

    result = qdr_cpivot(M);

    performance.end();
  }

template<typename uni10_type>
  void unitTestQDRCPIVOTinplace(uni10::Matrix<uni10_type> &M, decltype(qdr_cpivot(M)) &result, Perf &performance)
  {
    performance.start();

    qdr_cpivot(M, result.at(0), result.at(1), result.at(2), uni10::INPLACE);

    performance.end();
  }

#endif
