#ifndef __TEST_LINALG_H__
#define __TEST_LINALG_H__

#include "uni10.hpp"

#include <iostream>
#include <thread>
#include <vector>

#include "test_tool.h"
#include "unit_test/unit_test_linalg.h"

using namespace uni10;

template<typename uni10_type>
  void testSVD(uni10_uint64 Rnum, uni10_uint64 Cnum, uni10_uint64 ntest, bool inplace)
  {
    // Declare the perf object to store performance info.
    Perf performances;
    std::vector<std::vector<uni10_double64>> errors(3);

    for (int i = 0; i < ntest; ++i) {
        // Declare and initialize pending matrix and result.
        Matrix<uni10_type> M(Rnum, Cnum);
        uni10_rand(M, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock);
        std::vector<Matrix<uni10_type>> result(3);

        // Invoke a thread for testing.
        if (inplace) {
            std::thread test(unitTestSVDinplace<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        } else {
            std::thread test(unitTestSVD<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        }

        // Calculate errors.
        // * Error 1 = |I - U * U^T|
        // * Error 1 = |I - V * V^T|
        // * Error 2 = |M - U * S * V^T|
        errors.at(0).push_back(distToUnitary<uni10_type> (result.at(0)));
        errors.at(1).push_back(distToUnitary<uni10_type> (result.at(2)));
        errors.at(2).push_back(distToZero<uni10_type> (M - dot(dot(result.at(0), result.at(1)), result.at(2))));
    }

    std::vector<uni10_double64> error_avg;
    for (auto errors : errors)
        error_avg.push_back(std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size());

    std::cout << std::setw(5)  << Rnum << std::setw(5) << Cnum
              << std::setw(3)  << ntest << "|"
              << std::setw(8)  << std::fixed << std::setprecision(3) << performances.elapsed_time()
              << std::setw(8)  << performances.peak_mem() / 1024 << "|"
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(0)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(1)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(2)
              << std::endl;
  }

template<typename uni10_type>
  void testSDD(uni10_uint64 Rnum, uni10_uint64 Cnum, uni10_uint64 ntest, bool inplace)
  {
    // Declare the perf object to store performance info.
    Perf performances;
    std::vector<std::vector<uni10_double64>> errors(3);

    for (int i = 0; i < ntest; ++i) {
        // Declare and initialize pending matrix and result.
        Matrix<uni10_type> M(Rnum, Cnum);
        uni10_rand(M, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock);
        std::vector<Matrix<uni10_type>> result(3);

        // Invoke a thread for testing.
        if (inplace) {
            std::thread test(unitTestSDDinplace<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        } else {
            std::thread test(unitTestSDD<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        }

        // Calculate errors.
        // * Error 1 = |I - U * U^T|
        // * Error 1 = |I - V * V^T|
        // * Error 2 = |M - U * S * V^T|
        errors.at(0).push_back(distToUnitary<uni10_type> (result.at(0)));
        errors.at(1).push_back(distToUnitary<uni10_type> (result.at(2)));
        errors.at(2).push_back(distToZero<uni10_type> (M - dot(dot(result.at(0), result.at(1)), result.at(2))));
    }

    std::vector<uni10_double64> error_avg;
    for (auto errors : errors)
        error_avg.push_back(std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size());

    std::cout << std::setw(5)  << Rnum << std::setw(5) << Cnum
              << std::setw(3)  << ntest << "|"
              << std::setw(8)  << std::fixed << std::setprecision(3) << performances.elapsed_time()
              << std::setw(8)  << performances.peak_mem() / 1024 << "|"
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(0)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(1)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(2)
              << std::endl;
  }

template<typename uni10_type>
  void testQR(uni10_uint64 Rnum, uni10_uint64 Cnum, uni10_uint64 ntest, bool inplace)
  {
    // Declare the perf object to store performance info.
    Perf performances;
    std::vector<std::vector<uni10_double64>> errors(2);
    std::vector<bool> validities;

    for (int i = 0; i < ntest; ++i) {
        // Declare and initialize pending matrix and result.
        Matrix<uni10_type> M(Rnum, Cnum);
        uni10_rand(M, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock);
        std::vector<Matrix<uni10_type>> result(2);

        // Invoke a thread for testing.
        if (inplace) {
            std::thread test(unitTestQRinplace<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        } else {
            std::thread test(unitTestQR<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        }

        // Calculate errors.
        // * Error 1 = |I - Q * Q^T|
        // * Error 2 = |M - Q * R|
        errors.at(0).push_back(distToUnitary<uni10_type> (result.at(0)));
        errors.at(1).push_back(distToZero<uni10_type> (M - dot(result.at(0), result.at(1))));

        // Check if R is lower triangular matrix.
        validities.push_back(isUpTri<uni10_type>(result.at(1)));
    }

    std::vector<uni10_double64> error_avg;
    for (auto errors : errors)
        error_avg.push_back(std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size());

    bool validity = true;
    for (auto valid : validities)
        validity = validity && valid;

    std::cout << std::setw(5)  << Rnum << std::setw(5) << Cnum
              << std::setw(3)  << ntest << "|"
              << std::setw(8)  << std::fixed << std::setprecision(3) << performances.elapsed_time()
              << std::setw(8)  << performances.peak_mem() / 1024 << "|"
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(0)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(1)
              << std::setw(5) << (validity ? "OK" : "fail")
              << std::endl;
  }

template<typename uni10_type>
  void testQL(uni10_uint64 Rnum, uni10_uint64 Cnum, uni10_uint64 ntest, bool inplace)
  {
    // Declare the perf object to store performance info.
    Perf performances;
    std::vector<std::vector<uni10_double64>> errors(2);
    std::vector<bool> validities;

    for (int i = 0; i < ntest; ++i) {
        // Declare and initialize pending matrix and result.
        Matrix<uni10_type> M(Rnum, Cnum);
        uni10_rand(M, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock);
        std::vector<Matrix<uni10_type>> result(2);

        // Invoke a thread for testing.
        if (inplace) {
            std::thread test(unitTestQLinplace<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        } else {
            std::thread test(unitTestQL<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        }

        // Calculate errors.
        // * Error 1 = |I - Q * Q^T|
        // * Error 2 = |M - Q * L|
        errors.at(0).push_back(distToUnitary<uni10_type> (result.at(0)));
        errors.at(1).push_back(distToZero<uni10_type> (M - dot(result.at(0), result.at(1))));

        // Check if R is lower triangular matrix.
        validities.push_back(isDnTri<uni10_type>(result.at(1)));
    }

    std::vector<uni10_double64> error_avg;
    for (auto errors : errors)
        error_avg.push_back(std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size());

    bool validity = true;
    for (auto valid : validities)
        validity = validity && valid;

    std::cout << std::setw(5)  << Rnum << std::setw(5) << Cnum
              << std::setw(3)  << ntest << "|"
              << std::setw(8)  << std::fixed << std::setprecision(3) << performances.elapsed_time()
              << std::setw(8)  << performances.peak_mem() / 1024 << "|"
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(0)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(1)
              << std::setw(5) << (validity ? "OK" : "fail")
              << std::endl;
  }

template<typename uni10_type>
  void testRQ(uni10_uint64 Rnum, uni10_uint64 Cnum, uni10_uint64 ntest, bool inplace)
  {
    // Declare the perf object to store performance info.
    Perf performances;
    std::vector<std::vector<uni10_double64>> errors(2);
    std::vector<bool> validities;

    for (int i = 0; i < ntest; ++i) {
        // Declare and initialize pending matrix and result.
        Matrix<uni10_type> M(Rnum, Cnum);
        uni10_rand(M, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock);
        std::vector<Matrix<uni10_type>> result(2);

        // Invoke a thread for testing.
        if (inplace) {
            std::thread test(unitTestRQinplace<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        } else {
            std::thread test(unitTestRQ<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        }

        // Calculate errors.
        // * Error 1 = |I - Q * Q^T|
        // * Error 2 = |M - R * Q|
        errors.at(0).push_back(distToUnitary<uni10_type> (result.at(1)));
        errors.at(1).push_back(distToZero<uni10_type> (M - dot(result.at(0), result.at(1))));

        // Check if R is lower triangular matrix.
        validities.push_back(isUpTri<uni10_type>(result.at(0)));
    }

    std::vector<uni10_double64> error_avg;
    for (auto errors : errors)
        error_avg.push_back(std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size());

    bool validity = true;
    for (auto valid : validities)
        validity = validity && valid;

    std::cout << std::setw(5)  << Rnum << std::setw(5) << Cnum
              << std::setw(3)  << ntest << "|"
              << std::setw(8)  << std::fixed << std::setprecision(3) << performances.elapsed_time()
              << std::setw(8)  << performances.peak_mem() / 1024 << "|"
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(0)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(1)
              << std::setw(5) << (validity ? "OK" : "fail")
              << std::endl;
  }

template<typename uni10_type>
  void testLQ(uni10_uint64 Rnum, uni10_uint64 Cnum, uni10_uint64 ntest, bool inplace)
  {
    // Declare the perf object to store performance info.
    Perf performances;
    std::vector<std::vector<uni10_double64>> errors(2);
    std::vector<bool> validities;

    for (int i = 0; i < ntest; ++i) {
        // Declare and initialize pending matrix and result.
        Matrix<uni10_type> M(Rnum, Cnum);
        uni10_rand(M, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock);
        std::vector<Matrix<uni10_type>> result(2);

        // Invoke a thread for testing.
        if (inplace) {
            std::thread test(unitTestLQinplace<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        } else {
            std::thread test(unitTestLQ<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        }

        // Calculate errors.
        // * Error 1 = |I - Q * Q^T|
        // * Error 2 = |M - L * Q|
        errors.at(0).push_back(distToUnitary<uni10_type> (result.at(1)));
        errors.at(1).push_back(distToZero<uni10_type> (M - dot(result.at(0), result.at(1))));

        // Check if L is lower triangular matrix.
        validities.push_back(isDnTri<uni10_type>(result.at(0)));
    }

    std::vector<uni10_double64> error_avg;
    for (auto errors : errors)
        error_avg.push_back(std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size());

    bool validity = true;
    for (auto valid : validities)
        validity = validity && valid;

    std::cout << std::setw(5)  << Rnum << std::setw(5) << Cnum
              << std::setw(3)  << ntest << "|"
              << std::setw(8)  << std::fixed << std::setprecision(3) << performances.elapsed_time()
              << std::setw(8)  << performances.peak_mem() / 1024 << "|"
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(0)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(1)
              << std::setw(5) << (validity ? "OK" : "fail")
              << std::endl;
  }

template<typename uni10_type>
  void testQDR(uni10_uint64 Rnum, uni10_uint64 Cnum, uni10_uint64 ntest, bool inplace)
  {
    // Declare the perf object to store performance info.
    Perf performances;
    std::vector<std::vector<uni10_double64>> errors(2);
    std::vector<bool> validities;

    for (int i = 0; i < ntest; ++i) {
        // Declare and initialize pending matrix and result.
        Matrix<uni10_type> M(Rnum, Cnum);
        uni10_rand(M, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock);
        std::vector<Matrix<uni10_type>> result(3);

        // Invoke a thread for testing.
        if (inplace) {
            std::thread test(unitTestQDRinplace<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        } else {
            std::thread test(unitTestQDR<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        }

        // Calculate errors.
        // * Error 1 = |I - Q * Q^T|
        // * Error 2 = |M - Q * D * R|
        errors.at(0).push_back(distToUnitary<uni10_type> (result.at(0)));
        errors.at(1).push_back(distToZero<uni10_type> (M - dot(dot(result.at(0), result.at(1)), result.at(2))));

        // Check if R is lower triangular matrix.
        validities.push_back(isUpTri<uni10_type>(result.at(2)));
    }

    std::vector<uni10_double64> error_avg;
    for (auto errors : errors)
        error_avg.push_back(std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size());

    bool validity = true;
    for (auto valid : validities)
        validity = validity && valid;

    std::cout << std::setw(5)  << Rnum << std::setw(5) << Cnum
              << std::setw(3)  << ntest << "|"
              << std::setw(8)  << std::fixed << std::setprecision(3) << performances.elapsed_time()
              << std::setw(8)  << performances.peak_mem() / 1024 << "|"
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(0)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(1)
              << std::setw(5) << (validity ? "OK" : "fail")
              << std::endl;
  }

template<typename uni10_type>
  void testLDQ(uni10_uint64 Rnum, uni10_uint64 Cnum, uni10_uint64 ntest, bool inplace)
  {
    // Declare the perf object to store performance info.
    Perf performances;
    std::vector<std::vector<uni10_double64>> errors(2);
    std::vector<bool> validities;

    for (int i = 0; i < ntest; ++i) {
        // Declare and initialize pending matrix and result.
        Matrix<uni10_type> M(Rnum, Cnum);
        uni10_rand(M, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock);
        std::vector<Matrix<uni10_type>> result(3);

        // Invoke a thread for testing.
        if (inplace) {
            std::thread test(unitTestLDQinplace<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        } else {
            std::thread test(unitTestLDQ<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        }

        // Calculate errors.
        // * Error 1 = |I - Q * Q^T|
        // * Error 2 = |M - L * D * Q|
        errors.at(0).push_back(distToUnitary<uni10_type> (result.at(2)));
        errors.at(1).push_back(distToZero<uni10_type> (M - dot(dot(result.at(0), result.at(1)), result.at(2))));

        // Check if L is lower triangular matrix.
        validities.push_back(isDnTri<uni10_type>(result.at(0)));
    }

    std::vector<uni10_double64> error_avg;
    for (auto errors : errors)
        error_avg.push_back(std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size());

    bool validity = true;
    for (auto valid : validities)
        validity = validity && valid;

    std::cout << std::setw(5)  << Rnum << std::setw(5) << Cnum
              << std::setw(3)  << ntest << "|"
              << std::setw(8)  << std::fixed << std::setprecision(3) << performances.elapsed_time()
              << std::setw(8)  << performances.peak_mem() / 1024 << "|"
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(0)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(1)
              << std::setw(5) << (validity ? "OK" : "fail")
              << std::endl;
  }

template<typename uni10_type>
  void testQDRCPIVOT(uni10_uint64 Rnum, uni10_uint64 Cnum, uni10_uint64 ntest, bool inplace)
  {
    // Declare the perf object to store performance info.
    Perf performances;
    std::vector<std::vector<uni10_double64>> errors(2);
    std::vector<std::vector<bool>> validities(2);

    for (int i = 0; i < ntest; ++i) {
        // Declare and initialize pending matrix and result.
        Matrix<uni10_type> M(Rnum, Cnum);
        uni10_rand(M, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock);
        std::vector<Matrix<uni10_type>> result(3);

        // Invoke a thread for testing.
        if (inplace) {
            std::thread test(unitTestQDRCPIVOTinplace<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        } else {
            std::thread test(unitTestQDRCPIVOT<uni10_type>, std::ref(M), std::ref(result),
                            std::ref(performances));
            test.join();
        }

        // Calculate errors.
        // * Error 1 = |I - Q * Q^T|
        // * Error 2 = |M - Q * D * R|
        errors.at(0).push_back(distToUnitary<uni10_type> (result.at(0)));
        errors.at(1).push_back(distToZero<uni10_type> (M - dot(dot(result.at(0), result.at(1)), result.at(2))));

        // Check if D is Diagnal and the singular value is decreasing
        validities.at(0).push_back(true);
        for (decltype(result.at(1).row()) i = 1;
              i < min(result.at(1).row(), result.at(1).col()); ++i)
          if (abs(result.at(1)[i]) > abs(result.at(1)[i - 1])) validities.at(0).back() = false;

        // Check if R can be upper triangular matrix
        validities.at(1).push_back(true);
        std::vector<bool> zeroCnt(result.at(2).col() + 1, false);
        for (decltype(result.at(2).col()) i = 0;
            i < result.at(2).col() && validities.at(1).back(); ++i) {
          int count = 0;
          auto elem_ind = i;
          while (count < result.at(2).row() && result.at(2)[elem_ind] != 0.0) {
            elem_ind += result.at(2).col();
            count++;
          }
          while (count < result.at(2).row() && result.at(2)[elem_ind] == 0.0) {
            elem_ind += result.at(2).col();
            count++;
          }
          if (count != result.at(2).row()) validities.at(1).back() = false;
        }

    }

    std::vector<uni10_double64> error_avg;
    for (auto errors : errors)
        error_avg.push_back(std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size());

    std::vector<uni10_double64> validity(2, true);
    for (auto valid : validities.at(0))
        validity.at(0) = validity.at(0) && valid;
    for (auto valid : validities.at(1))
        validity.at(1) = validity.at(0) && valid;

    std::cout << std::setw(5)  << Rnum << std::setw(5) << Cnum
              << std::setw(3)  << ntest << "|"
              << std::setw(8)  << std::fixed << std::setprecision(3) << performances.elapsed_time()
              << std::setw(8)  << performances.peak_mem() / 1024 << "|"
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(0)
              << std::setw(10) << std::setprecision(3) << std::scientific << error_avg.at(1)
              << std::setw(5) << (validity.at(0) ? "OK" : "fail")
              << std::setw(5) << (validity.at(1) ? "OK" : "fail")
              << std::endl;
  }

#endif
