#include <chrono>

#include <unistd.h>
#include <sys/syscall.h>

#include <fstream>
#include <string>

#include "test_tool/test_perf.h"
#include "test_tool/test_systool.h"

using namespace std::chrono;

Perf::Perf() {

}

Perf::~Perf() {

}

void Perf::start() {
  _rss_init = 0;//getMaxRss();
  _clock_start = std::chrono::high_resolution_clock::now();
}

void Perf::end() {
  _clock_end = std::chrono::high_resolution_clock::now();
  _rss_peak = getMaxRss();
}

double Perf::elapsed_time() {
  return duration_cast<duration<double>>(_clock_end - _clock_start).count();
}

long int Perf::peak_mem() {
  return _rss_peak - _rss_init;
}
