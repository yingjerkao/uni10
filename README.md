# Universal Tensor Network Library (Uni10)

  [Uni10](https://uni10.gitlab.io) is an open-source C++ library designed for the development of
tensor network algorithms. Programming tensor network algorithms is
tedious and  prone to errors.  The task of keeping track of tensor
indices while performing contraction of a complicated tensor network
can be daunting. It is desirable to have a platform that provides
 bookkeeping capability and optimization.

  This software distinguishes itself from  other available software
solutions by providing the following advantages:

  * Fully implemented in C++.

  * Aimed toward applications in tensor network algorithms.

  * Provides basic tensor operations with an easy-to-use interface.

  * Provides a `Network` class to process and store the  details of the
    graphical representations of the networks.

  * Provides a collection of Python wrappers which interact with the
    Uni10 C++ library to take advantage of the Python language
    for better code readability and faster prototyping, without
    sacrificing the speed.

  * Provides behind-the-scene optimization and acceleration.

## Current Release

### Latest release: v2.0.0

### What's new?

  * Major API changes, `Matrix` and `UniTensor` are changed to class templates.
    Linear algebra operations are changed to global functions.

    see [API Changes](API_Changes.md) and
    [Uni10 APP Development Guide](DevGuide.md) for details.

  * Official CUDA GPU support for double precision.

  * New architecture design to allow support for different architectures and
    customized containers and linear algebra libraries.

  * Optimization of the network contraction engine to reduce the memory
    requirements and speedup computation.

  * PyUni10 build is moved out of the main tree. It will be supported via
    [Anaconda Cloud](https://anaconda.org/Uni10) and PiPy soon.

  * HDF5 support is removed temporarily and will be restored soon.


## Copyright and Changes

  See GPL and LGPL for copyright conditions.

  See [Release Notes](ChangeLog.md) for release notes and changes.


## Installation


### Download

The latest Uni10 source code can be downloaded from
<a href="https://gitlab.com/uni10/uni10" rel="nofollow" target="_blank">GitLab</a>.


### Requirements

  * <a href="http://cmake.org/" target="_blank">cmake</a> version > 2.8.12
  * C++ compiler with C++11 support

    * g++ >= 4.6.0
    * Intel C++ Compiler >= 15.0
    * Clang >= 9.0
    * Apple Clang >= 9.0
    * MSVC >=

  * BLAS and LAPACK libraries and header files
  * <a href="http://www.stack.nl/~dimitri/doxygen/" target="_blank">Doxygen</a> (for documentation)


### Build

 To build Un10, follow the following steps:

  1. Create a build directory

  2. Use `Cmake` to generate makefile

  3. Build library and exmamples

  4. Install library and examples (May require root access)

### Examples

Using system c++, blas and lapack

    > mkdir build
    > cd build
    > cmake </path/to/uni10/>
    > make
    > sudo make install

The installation path defaults to `/usr/local/uni10`.

To override the default path, use `CMAKE_INSTALL_PREFIX` :

    > cmake -DCMAKE_INSTALL_PREFIX=</installation_path> </path/to/uni10/>

To use MKL and Intel compiler:

    > cmake -DBUILD_WITH_MKL=on -DBUILD_WITH_INTEL_COMPILER=on </path/to/uni10/>

If cmake failes to find blas and lapack, specify the libraries by

    > cmake -DBLAS_LIBRARIES=</path/to/blas> -DLAPACK_LIBRARIES=</path/to/lapack> </path/to/uni10/>

### Build Options

 Option                       | Description (Default value)
----------------------------- | -------------------------------------------
 BUILD_WITH_MKL               | Use Intel MKL for lapack and blas (off)
 BUILD_WITH_INTEL_COMPILERS   | Use Intel C++ compiler  (off)
 BUILD_EXAMPLES               | Build C++ examples (on)
 BUILD_DOC                    | Build Documentation (off)
 BUILD_CUDA_SUPPORT           | Build Library for CUDA GPUs (off)
 CMAKE_INSTALL_PREFIX         | Installation location (/usr/local/uni10)


## Developers

### Contributors and maintainers

  * Ying-Jer Kao (National Taiwan University)

  * Pochung Chen (National Tsing-Hua University)

  * Yun-Hsuan Chou (National Taiwan University)

  * Kai-Hsin Wu (National Taiwan University)

  * Chih-Yuan Lee (National Taiwan University)

  * Chen-Yen Lai (University of California, Merced)

  * Yen-Hsin Wu (National Tsing-Hua University)

  * Chung-Yu Lo (National Tsing-Hua University)

  * Yi-Hao Jhu (National Tsing-Hua University)

  * Ian McCulloch (University of Queensland)
  
### Alumni

  * Yun-Da Hsieh (Code Base, GPU)

  * Tama Ma (CMake script)

  * Sukhbinder Singh (Matlab Wrapper)


## How to Contribute

  * Clone the project from [GitLab](https://gitlab.com/uni10/uni10) and use it.

  * Report bugs by creating issues at [Uni10 Repo](https://gitlab.com/uni10/uni10/issues/new?issue)

  * Fork us on [GitLab](https://gitlab.com/uni10/uni10).

  * Create Merge Requests.


## Known issues

  * CMake generated Xcode project fails to link.


## Links

  * [Uni10 Website](https://uni10.gitlab.io)

  * [Uni10 Repo](https://gitlab.com/uni10/uni10)



[![pipeline status](https://gitlab.com/uni10/uni10/badges/develop/pipeline.svg)](https://gitlab.com/uni10/uni10/commits/develop)

[![coverage report](https://gitlab.com/uni10/uni10/badges/develop/coverage.svg)](https://gitlab.com/uni10/uni10/commits/develop)
