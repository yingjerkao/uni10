#ifndef __ITEBD_TOOLS_H__
#define __ITEBD_TOOLS_H__

#include "uni10.hpp"

using namespace std;
using namespace uni10;

double Real(uni10_complex128 a){
  return a.real();
}

double Real(uni10_double64 a){
  return a;
}

template<typename T>
void bondcat(UniTensor<T>& Tout, UniTensor<T>& L, uni10_int bidx);

template<typename T>
void bondrm(UniTensor<T>& Tout, UniTensor<T>& L, uni10_int bidx);

template<typename T>
void svd_truncate(UniTensor<T>& theta, UniTensor<T>& ga, UniTensor<T>& gb, UniTensor<T>& la, long chi);

template<typename T>
void sv_merge(vector<T>& svs, vector<long>& bidxs, long bidx, Matrix<T>& sv_mat, long chi);

template<typename T>
void bondcat(UniTensor<T>& Tout, UniTensor<T>& L, uni10_int bidx){

  vector<uni10_int> labels = Tout.label();
  uni10_uint64 inBondNum = Tout.InBondNum();

  UniTensor<T> tmp;

  if(bidx == 0){

    int labelL[] = {labels[0], 77};
    int labelT[] = {77, labels[1], labels[2]};
    L.SetLabel(labelL);
    Tout.SetLabel(labelT);
    Contract(tmp, L, Tout, INPLACE);

  }
  else if(bidx == 1){

    int labelL[] = {77, labels[1]};
    int labelT[] = {labels[0], 77, labels[2]};
    L.SetLabel(labelL);
    Tout.SetLabel(labelT);
    Contract(tmp, Tout, L, INPLACE);

  }
  else{

    fprintf(stdout, "\nIn bondcat(): Invalid Usage.");
    fprintf(stdout, "\n");

  }

  Permute(Tout, tmp, labels, inBondNum, INPLACE);

}

template<typename T>
void bondrm(UniTensor<T>& Tout, UniTensor<T>& L, uni10_int bidx){

  UniTensor<T> invL = L;
  T* elem = invL.GetElem();
  for(uni10_uint64 i=0; i!=L.ElemNum(); i++){
    elem[i] = elem[i] == 0.0 ? 0.0 : ( 1.0 / elem[i]);
  }
  bondcat(Tout, invL, bidx);

}

template<typename T>
void svd_truncate(UniTensor<T>& theta, UniTensor<T>& ga, UniTensor<T>& gb, UniTensor<T>& la, long chi){

  map< Qnum, vector< Matrix<T> > > svds;
  vector<Qnum> blk_qnums = theta.BlocksQnum();
  for(vector<Qnum>::iterator q = blk_qnums.begin(); q != blk_qnums.end(); q++)
    svds[*q] = Svd(theta.GetBlock(*q));

  vector<T> svs;
  vector<long> bidxs;
  for(long bidx = 0; bidx < blk_qnums.size(); bidx++)
    sv_merge(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi);

  vector<long>dims(blk_qnums.size(), 0);
  for(int i = 0; i < bidxs.size(); i++)
    dims[bidxs[i]]++;

  vector<Qnum> qnums(svs.size());
  int cnt = 0;
  for(size_t bidx = 0; bidx < blk_qnums.size(); bidx++){
    for(int i = 0; i < dims[bidx]; i++){
      qnums[cnt] = blk_qnums[bidx];
      cnt++;
    }
  }

  Bond bdi_mid = Bond(BD_IN, qnums);
  Bond bdo_mid = Bond(BD_OUT, qnums);

  vector<Bond> ga_bonds = ga.bond();
  vector<Bond> gb_bonds = gb.bond();
  vector<Bond> la_bonds;
  ga_bonds[2] = bdo_mid;
  gb_bonds[0] = bdi_mid;

  la_bonds.push_back(bdi_mid);
  la_bonds.push_back(bdo_mid);

  vector<int> ga_labels = ga.label();
  ga.Assign(ga_bonds);
  gb.Assign(gb_bonds);
  la.Assign(la_bonds);
  ga.SetLabel(ga_labels);

  map<Qnum, int> degs = bdi_mid.degeneracy();
  Matrix<T> sv_mat(bdi_mid.dim(), bdo_mid.dim(), true);
  sv_mat.SetElem(svs);

  double norm = Norm(sv_mat);
  for(map<Qnum, int>::iterator it = degs.begin(); it != degs.end(); it++){
    typename map<Qnum, vector<Matrix<T> > >::iterator sit = svds.find(it->first);
    if(sit == svds.end()){
      fprintf(stdout, "\nFatal Error in setTruncation()");
      exit(0);
    }
    Matrix<T> tga, tgb, tla;
    Resize(tga, sit->second[0], sit->second[0].row(), it->second, INPLACE);
    Resize(tgb, sit->second[2], it->second, sit->second[2].col(), INPLACE);
    Resize(tla, sit->second[1], it->second, it->second, INPLACE);
    ga.PutBlock(it->first, tga);
    gb.PutBlock(it->first, tgb);
    la.PutBlock(it->first, tla * (1/norm));
  }

}

template<typename T>
void sv_merge(vector<T>& svs, vector<long>& bidxs, long bidx, Matrix<T>& sv_mat, long chi){

  if(svs.size()){
    int len = svs.size() + sv_mat.ElemNum();
    len = len < chi ? len : chi;
    vector<T> ori_svs = svs;
    vector<long> ori_bidxs = bidxs;
    svs.assign(len, 0);
    bidxs.assign(len, 0);
    int cnt = 0;
    int cur1 = 0;
    int cur2 = 0;
    while(cnt < len){
      if(cur1 < ori_svs.size() && cur2 < sv_mat.ElemNum()){
        if(Real(ori_svs[cur1]) >= Real(sv_mat[cur2])){
          svs[cnt] = ori_svs[cur1];
          bidxs[cnt] = ori_bidxs[cur1];
          cur1++;
        }
        else{
          svs[cnt] = sv_mat[cur2];
          bidxs[cnt] = bidx;
          cur2++;
        }
      }
      else if(cur2 < sv_mat.ElemNum()){
        for(; cnt < svs.size(); cnt++){
          svs[cnt] = sv_mat[cur2];
          bidxs[cnt] = bidx;
          cur2++;
        }
        break;
      }
      else{
        for(; cnt < svs.size(); cnt++){
          svs[cnt] = ori_svs[cur1];
          bidxs[cnt] = ori_bidxs[cur1];
          cur1++;
        }
        break;
      }
      cnt++;
    }
  }
  else{
    bidxs.assign(sv_mat.ElemNum(), bidx);
    svs.assign(sv_mat.ElemNum(), 0);
    for(int i = 0; i < sv_mat.ElemNum(); i++)
      svs[i] = sv_mat[i];
  }

}

#endif
