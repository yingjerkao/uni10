#include "bmps_tools.h"
#include "bmps.h"

template<typename T>
twoSiteiMPS<T>::twoSiteiMPS( const int dimMPSIn, const vector<UniTensor<T>>& evolOperatorIn ): dimMPS{dimMPSIn}, dimPhy( evolOperatorIn.at(0).bond(0).dim() ), evolOperator( evolOperatorIn) {
  //init gammas
  vector<Bond> gamma_bds(3);
  gamma_bds[0] = Bond( BD_IN, dimPhy);
  gamma_bds[1] = Bond( BD_OUT, dimMPS);
  gamma_bds[2] = Bond( BD_OUT, dimMPS);
  UniTensor<T> oneGamma( gamma_bds );
  vector<T> productElem( oneGamma.ElemNum(), 0 );
  productElem.at(0) = 1.0;
  oneGamma.SetElem( productElem );
  gammas = vector< UniTensor<T> > ( 2, oneGamma );
  //inti lambdas
  Matrix<T> oneLambda( dimMPS, dimMPS, true );
  uni10_rand( oneLambda, uni10_mt19937, uni10_uniform_real, 0, 1, uni10_clock);
  productElem = vector<T> ( dimMPS, 0 );
  productElem.at(0) = 1.0;
  oneLambda.SetElem( productElem );
  lambdas = vector<Matrix<T> > ( 2, oneLambda );

  gammaLBalance = gammas.at(0);
  bondsqrtcat( gammaLBalance, lambdas[1], 1 );
  gammaRBalance = gammas.at(1);
  bondsqrtrm( gammaRBalance, lambdas[1], 2 );
}

template<typename T>
UniTensor<T> twoSiteiMPS<T>::contractThetaBalance( const int iSite){
  assert( iSite==0||iSite==1 );

  UniTensor<T> theta = contractCten( iSite );
  bondsqrtcat( theta, lambdas[1-iSite], 0 );
  bondsqrtrm(  theta, lambdas[1-iSite], 1 );

  return theta;
}

template<typename T>
UniTensor<T> twoSiteiMPS<T>::contractCten( const int iSite){
  assert( iSite==0||iSite==1 );
  vector<UniTensor<T>> gammaNow = gammas;

  int i_l = iSite, i_r = 1-iSite;

  int gl_label[] = {4, 1, 2};
  int gr_label[] = {5, 2, 3};

  gammaNow[i_l].SetLabel(gl_label);
  gammaNow[i_r].SetLabel(gr_label);
  
  UniTensor<T> theta = Contract( gammaNow[i_l], gammaNow[i_r], false );//now theta label 4, 1; 5, 3
  theta = Permute( theta, { 1, 3, 4, 5}, 2 );
  return theta;
}

template<typename T>
UniTensor<T> twoSiteiMPS<T>::contractTheta( const int iSite){
  assert( iSite==0||iSite==1 );
  UniTensor<T> theta = contractCten( iSite );//now theta label 1, 3; 4, 5
  bondcat( theta, lambdas[(1-iSite)], 0 );
  return theta;
}

template<typename T>
double twoSiteiMPS<T>::timeEvoAstep( UniTensor<T> &evolOperatorTemp, const int iSite){
  assert( iSite==0||iSite==1 );
  int i_l = iSite, i_r = 1-iSite;

  UniTensor<T> cTen = contractCten( iSite );//now cTen label 1, 3; 4, 5
  cTen = Contract( cTen, evolOperatorTemp, false );//now theta label 1, 3; 6, 7
  cTen = Permute( cTen, {6, 1, 7, 3}, 2  );
  UniTensor<T> theta = cTen;
  bondcat( theta, lambdas[i_r], 1 );//label 6,1;7,3

  //svd and update
  vector<Matrix<T> > usv = Sdd(theta.GetBlock());
  usv[1] *= 1.0/Norm( usv[1] );
  double trunErr = 0;
  for ( int i=dimMPS; i!=usv[1].col(); i++ ){
    //trunErr += pow( usv[1].at( i, i ), 2);
  }
  Resize( usv[1],          dimMPS,       dimMPS, INPLACE);
  Resize( usv[2],          dimMPS, usv[2].col(), INPLACE);

  //update lambda
  lambdas[i_l] = usv[1];
  double normFactor = 1.0/Norm( lambdas[i_l] );
  lambdas[i_l] *= normFactor;

  //update right tensor
  gammas[i_r].SetLabel( {5, 2, 3});
  gammas[i_r] = Permute( gammas[i_r], {2,5,3}, 1);
  gammas[i_r].PutBlock( usv[2]);
  gammas[i_r] = Permute( gammas.at(i_r), {5,2,3}, 1);

  //update right tensor using Hasings technique
  UniTensor<T> temp = Dagger(gammas[i_r]);
  temp.SetLabel({-2,3,7});
  gammas[i_l] = Contract( cTen, temp, false );
  gammas[i_l] = Permute( gammas[i_l], {6,1,-2}, 1 );
  gammas[i_l].SetLabel({4,1,2});
  gammas[i_l] *= normFactor;
  return trunErr;
}

template<typename T>
bool twoSiteiMPS<T>::fixedPoint( const int maxIter, const double errTor, double &trunErr ){
  evolOperator.at(0).SetLabel({4, 5, 6, 7});
  evolOperator.at(1).SetLabel({4, 5, 6, 7});

  vector<Matrix<T>> lambdaOlds = lambdas;
  cout<<endl<<"finding fixed point"<<endl;
  cout<<setw(8)<<"istep"<<setw(12)<<"diff"<<endl;
  fflush(stdout);
  double trunErr1, trunErr2;
  for ( int istep=0; istep!=maxIter; istep++){
    trunErr1 = timeEvoAstep( evolOperator.at(0), 0 );
    trunErr2 = timeEvoAstep( evolOperator.at(1), 1 );
    trunErr = max( trunErr1, trunErr2);
    double diff0 = pow( Norm( lambdaOlds[0] + (-1.0)*lambdas[0] ), 2 )/dimMPS;
    double diff1 = pow( Norm( lambdaOlds[1] + (-1.0)*lambdas[1] ), 2 )/dimMPS;
    double diff = max( diff0, diff1 );
    
    if (istep%5==0){
      cout<<setw(8)<<istep<<setw(12)<<setprecision(4)<<diff<<endl;
      fflush(stdout);
    } else { }

    if (diff>errTor){
      lambdaOlds = lambdas;
    }
    else {
      cout<<setw(8)<<istep<<setw(12)<<setprecision(4)<<diff<<endl;
      cout<<"converge to fixed point"<<endl;
      fflush(stdout);
      gammaLBalance = gammas.at(0);
      bondsqrtcat( gammaLBalance, lambdas[1], 1 );
      gammaRBalance = gammas.at(1);
      bondsqrtrm( gammaRBalance, lambdas[1], 2 );
      return true;
    }
  }
  cout<<"cannot converge to fixed point"<<endl;
  fflush(stdout);
  return false; 
}

template<typename T>
T twoSiteiMPS<T>::measureTwoSite( UniTensor<T> &twoSiteOp, const int iSite ){
  assert( iSite==0||iSite==1 );
  int i_l = iSite, i_r = 1-iSite;

  UniTensor<T> theta = contractTheta( iSite );//now theta label 1, 3; 4, 5
  UniTensor<T> thetaT = Dagger( theta ); //thetaT label 4, 5; 1, 3
  thetaT.SetLabel( { 6, 7, 1, 3} );//prepare for contraction
  twoSiteOp.SetLabel( { 4, 5, 6, 7,} );
  UniTensor<T> thetaH = Contract( theta, twoSiteOp, false ); //theta label 1, 3; 6, 7
  theta.SetLabel( {1, 3, 6, 7});

  T norm  = Contract( theta, thetaT, false )[0];
  T expec = Contract( theta, thetaH, false )[0];

  return expec/norm;
}

template<typename T>
Bmps<T>::Bmps(const UniTensor<T>& _H,  const bmps_paras& paras): H(_H), 
transferMatrix_net( Network("./bmps_tools/Networks/transferMatrix.net") ),
bmpsNorm_net(       Network("./bmps_tools/Networks/bmpsNorm.net") ),
bmpsExpec_net(      Network("./bmps_tools/Networks/bmpsExpec.net") )
{

  fprintf(stdout, "\n");
  paras.print_info();
  fprintf(stdout, "\n");

  cout << H;

  dim = H.bond(0).dim();
  D = paras.D;
  max_N   = paras.max_N;
  eps     = paras.eps;
  cut_off = paras.cut_off;
  measure_per_n_iter = paras.measure_per_n_iter;

  chi = (uni10_int)cut_off == -1 ? D : paras.chi;

}

template<typename T>
void Bmps<T>::initialize( const string& loadDir){
  LoadTensors(loadDir);
  bondsqrtcatAll();
  UniTensor<T> gateA = contractSelf( gammas[0], {0} );
  combineTwoLayer(gateA);
  UniTensor<T> gateB = contractSelf( gammas[1], {0} );
  combineTwoLayer(gateB);

  gateA.SetLabel({1,2,3,4});
  gateA = Permute( gateA, {1,2,4,3}, 2 );
  gateB.SetLabel({1,2,3,4});
  gateB = Permute( gateB, {3,4,2,1}, 2 );
  vector<UniTensor<T>> evolUp = {gateA, gateB};

  gateA = Permute( gateA, {3,4,2,1}, 2 );
  gateB = Permute( gateB, {1,2,4,3}, 2 );
  vector<UniTensor<T>> evolDn = {gateB, gateA};

  mpsUp = twoSiteiMPS<T> ( chi, evolUp );
  mpsDn = twoSiteiMPS<T> ( chi, evolDn );
}

template<typename T>
void Bmps<T>::LoadTensors(const string& loadDir){
  gammas = vector<UniTensor<T>> (2);
  lambdas = vector<Matrix<T>> (4);
  for ( int i=0; i<gammas.size(); i++ ){
    char buffer[32];
    sprintf( buffer, "%s%d.ten", "Gamma", i );
    string LoadPath = loadDir + string(buffer);
    gammas.at(i).Load(LoadPath);
  }
  for ( int i=0; i<lambdas.size(); i++ ){
    char buffer[32];
    sprintf( buffer, "%s%d.mat", "Lambda", i );
    string LoadPath = loadDir + string(buffer);
    lambdas.at(i).Load(LoadPath);
  }
}

template<typename T>
Bmps<T>::~Bmps(){


}

// Set hamiltonian in iTEBD algorithm.
template<typename T>
void Bmps<T>::setHamiltonian(const UniTensor<T>& _H){

  H = _H;

}

template<typename T>
void Bmps<T>::Optimize(){
  double trunErrUp, trunErrDn;
  mpsUp.fixedPoint( max_N, eps, trunErrUp );
  mpsDn.fixedPoint( max_N, eps, trunErrDn );
  double trunErr = max(trunErrUp, trunErrDn);

  //contract transfer Matrix
  UniTensor<T> transferMatrix;
  ContractArgs( transferMatrix, transferMatrix_net, mpsUp.getGammaLBalance(), mpsUp.getGammaRBalance(), mpsDn.getGammaLBalance(), mpsDn.getGammaRBalance(), mpsUp.getEvolOperator()[0], mpsUp.getEvolOperator()[1] );
  vector<Bond> traBds = transferMatrix.bond();
  vector<Bond> vecBds( traBds.begin(), traBds.begin()+3 );

  vector<Matrix<complex<double>>> rigEigRes = Eig( transferMatrix.GetBlock() );
  Matrix<complex<double>> rigVec = Inverse( rigEigRes[1] );
  Resize( rigVec, rigVec.row(), 1, INPLACE );
  rigTen = UniTensor<complex<double>> (vecBds);
  rigTen.PutBlock( rigVec );

  vector<Matrix<complex<double>>> lefEigRes = Eig( Transpose(transferMatrix.GetBlock()) );
  Matrix<complex<double>> lefVec = Inverse( lefEigRes[1] );
  Resize( lefVec, lefVec.row(), 1, INPLACE );
  lefTen = UniTensor<complex<double>> (vecBds);
  lefTen.PutBlock( lefVec );

  measureNorm();
}

template<typename T>
void Bmps<T>::bondsqrtcatAll( ){
  uni10_int32 i_l =0, i_r =1;
  enum{ bond_phy, bond_l, bond_u, bond_r, bond_d};//left,up,right,down    

  for( int i=bond_l; i<= bond_d; i++){
    bondsqrtcat( gammas[i_l], lambdas[i-1], i);
  }
  for( int j=bond_l; j<= bond_d; j++){
    bondsqrtcat( gammas[i_r], lambdas[j-1], j);
  }
}

template<typename T>
void Bmps<T>::measureNorm( ){
  UniTensor<complex<double>> bmpsNorm;
  ContractArgs( bmpsNorm, bmpsNorm_net, mpsUp.getGammaLBalance(), mpsUp.getGammaRBalance(), mpsDn.getGammaLBalance(), mpsDn.getGammaRBalance(), mpsUp.getEvolOperator()[0], mpsUp.getEvolOperator()[1], rigTen, lefTen );
  assert( bmpsNorm[0].imag()<1.0e-14 );
  currentNorm = bmpsNorm[0].real();
}

template<typename T>
double Bmps<T>::measureExpec( const UniTensor<T> &twoSiteOp ){
  gammas[0].SetLabel({14,6,4,7,10});
  gammas[1].SetLabel({15,8,11,7,5});
  UniTensor<T> theta = Contract( gammas[0], gammas[1], false );
  theta = Permute( theta, {6,4,5,8,11,10,14,15}, 6 );
  UniTensor<T> thetaT = Conj( theta );
  thetaT.SetLabel({-6,-4,-5,-8,-11,-10,-14,-15});
  UniTensor<T> opCopy = twoSiteOp;
  opCopy.SetLabel({14,15,-14,-15});
  theta = Contract( theta, opCopy, false );
  theta = Contract( theta, thetaT, false );

  vector<int> comLab(2);
  int inBdN = theta.InBondNum();
  for (int i=0; i!=inBdN; i++){
    comLab[0] =  theta.label()[i];
    comLab[1] = -theta.label()[i];
    theta.CombineBond( comLab );
  }

  UniTensor<T> bmpsExpec;
  ContractArgs( bmpsExpec, bmpsExpec_net, mpsUp.getGammaLBalance(), mpsUp.getGammaRBalance(), mpsDn.getGammaLBalance(), mpsDn.getGammaRBalance(), theta, rigTen, lefTen );
  complex<double> compExpec = bmpsExpec[0];
  assert( compExpec.imag()<1.0e-14 );
  return compExpec.real()/currentNorm;
}

template class twoSiteiMPS<uni10_double64>;
template class twoSiteiMPS<uni10_complex128>;
template class Bmps<uni10_double64>;
template class Bmps<uni10_complex128>;
